import React from "react";
import { Route } from "react-router-dom";
import "./App.css";
import Aside from "./components/Aside/Aside";
import Dialogs from "./components/Dialogs/Dialogs";
import Header from "./components/Header/Header";
import News from "./components/News/News";
import Profile from "./components/Profile/Profile";

function App(props) {
  return (
    <div className="app-wrapper">
      <Header />
      <Aside />
      <div className="appWrapperContent">
        {/* <Route path='/profile' component={Profile} />
          <Route path='/dialogs' component={Dialogs} />
          <Route path='/news' component={News} /> */}
        <Route
          path="/profile"
          render={() => <Profile store={props.store} />}
        />
        <Route path="/dialogs" render={() => <Dialogs store={props.store} />} />
        <Route path="/news" render={() => <News />} />
      </div>
    </div>
  );
}

export default App;
