import profileReducer from "./profile-reducer";
import dialogsReducer from "./dialogs-reducer";
import sidebarReducer from "./sidebar-reducer";

let store = {
  _state: {
    profilePage: {
      postsData: [
        {
          id: 1,
          message: "How are you?",
          likeCounter: 15,
          avatar: "https://avatarko.ru/img/kartinka/19/kot_eda_18744.jpg",
        },
        {
          id: 2,
          message: "Happy Holidays",
          likeCounter: 20,
          avatar: "https://avatarko.ru/img/kartinka/12/fantastika_11292.jpg",
        },
        {
          id: 3,
          message: "Merry Christmas",
          likeCounter: 5,
          avatar: "https://avatarko.ru/img/kartinka/33/fantastika_vampir_32247.jpg",
        },
        {
          id: 4,
          message: "My first post",
          likeCounter: 24,
          avatar: "https://avatarko.ru/img/kartinka/13/fantastika_vampir_letuchaya_mysh_12979.jpg",
        },
      ],
      newPostText: "it-camasutra.com",
    },
    dialogsPage: {
      dialogsData: [
        { id: 1, name: "Jonh" },
        { id: 2, name: "Pablo" },
        { id: 3, name: "Michael" },
        { id: 4, name: "Valera" },
        { id: 5, name: "Illya" },
        { id: 6, name: "Oleg" },
      ],
      messadgesData: [
        { id: 1, massedges: "Yo" },
        { id: 2, massedges: "How are you?" },
        { id: 3, massedges: "How are you?" },
        { id: 4, massedges: "How are you?" },
        { id: 5, massedges: "How are you?" },
        { id: 6, massedges: "Hi" },
      ],
      newMessagesBody: "",
    },
    sidebar: {

    },
  },
  get state() {
    return this._state;
  },
  getstate() {
    return this._state;
  },
  _callSubscriber() {
    console.log("state was changed");
  },
  subscribe(observer) {
    this._callSubscriber = observer; //наблюдатель // publisher-subscriber // adEventListner
  },
  dispatch(action) {
    this._state.profilePage = profileReducer(this._state.profilePage, action);
    this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action);
    this._state.sidebar = sidebarReducer(this._state.sidebar, action);
    
    this._callSubscriber(this._state);
  },
  //addMessage(message) {
  //  this._store.dialogsPage.massedgesData.push(message);
  //},
  //selectDialogUser(userId) {
  //  this._store.selectedUserId = userId;
  //},
};

export default store;
window.store = store;

// store - OOP
