const UPDATE_NEW_MESSADGE_BODY = "UPDATE-NEW-MESSADGE-BODY";
const SEND_MESSADGE = "SEND-MESSADGE";

let initialState = {
  dialogsData: [
    { id: 1, name: "Jonh" },
    { id: 2, name: "Pablo" },
    { id: 3, name: "Michael" },
    { id: 4, name: "Valera" },
    { id: 5, name: "Illya" },
    { id: 6, name: "Oleg" },
  ],
  messadgesData: [
    { id: 1, massedges: "Yo" },
    { id: 2, massedges: "How are you?" },
    { id: 3, massedges: "How are you?" },
    { id: 4, massedges: "How are you?" },
    { id: 5, massedges: "How are you?" },
    { id: 6, massedges: "Hi" },
  ],
  newMessagesBody: "",
};

const dialogsReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_NEW_MESSADGE_BODY:
      state.newMessagesBody = action.body;
      return state;

    case SEND_MESSADGE:
      let body = state.newMessagesBody;
      state.newMessagesBody = "";
      state.messadgesData.push({ id: 6, massedges: body });
      return state;

    default:
      return state;
  }
};

export const sendMessadgeCreator = () => ({ type: SEND_MESSADGE });

export const updateNewMessadgeBodyCreator = (text) => ({
  type: UPDATE_NEW_MESSADGE_BODY,
  body: text,
});

export default dialogsReducer;
