const ADD_POST = "ADD-POST";
const UPDATE_NEW_POST_TEXT = "UPDATE-NEW-POST-TEXT";

let initialState = {
  postsData: [
    {
      id: 1,
      message: "How are you?",
      likeCounter: 15,
      avatar: "https://avatarko.ru/img/kartinka/19/kot_eda_18744.jpg",
    },
    {
      id: 2,
      message: "Happy Holidays",
      likeCounter: 20,
      avatar: "https://avatarko.ru/img/kartinka/12/fantastika_11292.jpg",
    },
    {
      id: 3,
      message: "Merry Christmas",
      likeCounter: 5,
      avatar: "https://avatarko.ru/img/kartinka/33/fantastika_vampir_32247.jpg",
    },
    {
      id: 4,
      message: "My first post",
      likeCounter: 24,
      avatar: "https://avatarko.ru/img/kartinka/13/fantastika_vampir_letuchaya_mysh_12979.jpg",
    },
  ],
  newPostText: "it-camasutra.com",
};

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_POST:
      let newPost = {
        id: 5,
        message: state.newPostText,
        likeCounter: 15,
        avatar: "https://avatarko.ru/img/kartinka/13/fantastika_vampir_letuchaya_mysh_12979.jpg",
      };
      state.postsData.push(newPost);
      state.newPostText = "";
      return state;

    case UPDATE_NEW_POST_TEXT:
      state.newPostText = action.newText;
      return state;

    default:
      return state;
  }
};

export const addPostActionCreator = () => ({ type: ADD_POST });

export const updateNewPostTextActionCreator = (text) => ({
  type: UPDATE_NEW_POST_TEXT,
  newText: text,
});

export default profileReducer;
