import { connect } from "react-redux";
import Interlocutors from "./Interlocutors";

let mapStateToProps = (state) => {
  return {
    state: state.dialogsPage.dialogsData,
  };
};
let mapDispatchToProps = () => {
  return {};
};

const InterlocutorsContainer = connect(mapStateToProps, mapDispatchToProps)(Interlocutors);

export default InterlocutorsContainer;
