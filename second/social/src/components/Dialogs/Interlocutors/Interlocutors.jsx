import React from "react";
import Interlocutor from "./Interlocutor/Interlocutor";
import s from "./Interlocutors.module.css";

const Interlocutors = (props) => {
  let dialogsElements = props.state.map((dialog) => (
    <Interlocutor name={dialog.name} id={dialog.id} />
  ));
  return <section className={s.interlocutors}>{dialogsElements}</section>;
};
export default Interlocutors;
