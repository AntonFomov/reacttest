import React from "react";
import { NavLink } from "react-router-dom";
import s from "./Interlocutor.module.css";

const Interlocutor = (props) => {
  return (
    <div className={s.interlocutor}>
      <NavLink className={s.link} activeClassName={s.active} to={'/dialogs/' + props.id}>
        <p>{props.name}</p>
      </NavLink>
    </div>
  );
};
export default Interlocutor;
