import React from "react";
import Message from "./Message/Messages";
import s from "./Messages.module.css";

const Messages = (props) => {
  let onSendMessadgeClick = () => {
    props.sendMessadge();
  };
  let onNewMessadgeChange = (e) => {
    let body = e.target.value;
    props.updateNewMessadgeBody(body);
  };
  let newMessadgeBody = props.newMessagesBody;
  let massedgesElement = props.messadgesData.map((massedge) => (
    <Message text={massedge.massedges} />
  ));

  return (
    <div className={s.messages}>
      {massedgesElement}
      <textarea
        placeholder="Enter your messadge"
        value={newMessadgeBody}
        onChange={onNewMessadgeChange}
      ></textarea>
      <button onClick={onSendMessadgeClick}> Send message</button>
    </div>
  );
};
export default Messages;
