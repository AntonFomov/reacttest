import { connect } from "react-redux";
import { sendMessadgeCreator, updateNewMessadgeBodyCreator } from "../../../redux/dialogs-reducer";
import Messages from "./Messages";

let mapStateToProps = (state) => {
  return {
    messadgesData: state.dialogsPage.messadgesData,
    newMessagesBody: state.dialogsPage.newMessagesBody,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    sendMessadge: () => {
      dispatch(sendMessadgeCreator());
    },
    updateNewMessadgeBody: (body) => {
      dispatch(updateNewMessadgeBodyCreator(body));
    },
  };
};
const MessagesContainer = connect(mapStateToProps, mapDispatchToProps)(Messages);

export default MessagesContainer;
