import React from "react";
import s from "./Dialogs.module.css";
import InterlocutorsContainer from "./Interlocutors/InterlocutorsContainer";
import MessagesContainer from "./Messages/MessagesContainer";

const Dialogs = (props) => {
  return (
    <section className={s.dialogs}>
      <InterlocutorsContainer store={props.store}/>
      <MessagesContainer store={props.store}/>
    </section>
  );
};
export default Dialogs;
