import React from "react";
import s from "./UserInfo.module.css";

const UserInfo = () => {
  return (
    <div className={s.UserInfo}>
    <img
        className={s.content__main_img}
        src="https://vjoy.cc/wp-content/uploads/2019/07/1.jpeg"
        alt=""
      />
      <div className={s.user}>
        <div className={s.user__photo}>
          <a>
            <img src="https://avatarko.ru/img/kartinka/5/kot_multfilm_4689.jpg" alt="" />
          </a>
        </div>
        <div className={s.user__name_wrapper}>
          <div className={s.user__name}>Dima Dima</div>
          <div className={s.user__description}>
            <ul>
              <li>
                <p>Date of Birth:</p>
              </li>
              <li>
                <p>City:</p>
              </li>
              <li>
                <p>Education:</p>
              </li>
              <li>
                <p>Web-Site:</p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};
export default UserInfo;
