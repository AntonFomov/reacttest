import React from "react";
import MyPostsContainer from "./MyPosts/MyPostsContainer";
import s from "./Profile.module.css";
import UserInfo from "./UserInfo/UserInfo";

const Profile = (props) => {
  console.log(props);
  return (
    <section className={s.content}>
      <UserInfo/>
      <MyPostsContainer store={props.store}/>
    </section>
  );
};
export default Profile;
