import React from "react";
import s from "./Post.module.css";

const Post = (props) => {
  return (
    <div className={s.item}>
      <div className={s.item__messadge}>
        <img
          src={props.avatar}
          alt=""
        />
        <p>{props.message}</p>
      </div>
      <span>Like {props.likeCounter}</span>
    </div>
  );
};
export default Post;
