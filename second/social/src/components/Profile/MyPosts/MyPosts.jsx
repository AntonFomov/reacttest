import React from "react";
import s from "./MyPosts.module.css";
import Post from "./Post/Post";

const MyPosts = (props) => {
  
  let postsElements = props.posts.map((post) => (
    <Post message={post.message} likeCounter={post.likeCounter} avatar={post.avatar} />
  ));
  let newPostElement = React.createRef();
  let onAddPost = () => {
    props.addPost();
  };
  let onPostChange = () => {
    let text = newPostElement.current.value;
    props.updateNewPostText(text);
  };
  console.log(props);
  return (
    <div className={s.postsBlock}>
      <div className={s.search}>
        <textarea ref={newPostElement} value={props.newPostText} onChange={onPostChange} />
        <button onClick={onAddPost}>Add post</button>
      </div>
      <div className={s.myPosts}>{postsElements}</div>
    </div>
  );
};
export default MyPosts;
