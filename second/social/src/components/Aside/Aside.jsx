import React from "react";
import { NavLink } from "react-router-dom";
import s from "./Aside.module.css";

const Aside = () => {
  return (
    <aside className={s.aside}>
      <ul>
        <li className={s.item}>
          <NavLink to="/profile" activeClassName={s.active}>Profile</NavLink>
        </li>
        <li className={s.item}>
          <NavLink to="/dialogs" activeClassName={s.active}>Messages</NavLink>
        </li>
        <li className={s.item}>
          <NavLink to="/news" activeClassName={s.active}>News</NavLink>
        </li>
        {/* <li className={s.item}>
          <a href="#">Music</a>
        </li>
        <li className={`${s.item} ${s.active}`}>
          <a href="#">Settings</a>
        </li> */}
      </ul>
    </aside>
  );
};
export default Aside;
