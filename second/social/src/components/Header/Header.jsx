import React from 'react';
import s from "./Header.module.css";

const Header = () => {
  return (
    <header className={s.header}>
      <img
        src="https://st2.depositphotos.com/1819500/5302/v/600/depositphotos_53025221-stock-illustration-cat-logo.jpg"
        alt=""
      />
    </header>
  );
};
export default Header;
