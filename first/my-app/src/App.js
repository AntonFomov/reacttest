import logo from "./logo.svg";
import React from 'react';
import openGoogleMap from "./googleMaps/google";
import "./App.css";
import Header from "./Header";
import Technologys from "./Technologys";
import Footer from "./Footer";

const App = () => {
  return (
    <div className="App">
      <Header />
      <Technologys />
      <Footer />
    </div>
  );
};
openGoogleMap();



//function App() {
//  return (
//    <div className="App">
//      <header className="App-header">
//        <img src={logo} className="App-logo" alt="logo" />
//        <p>
//          Some test text <code>src/App.js</code> and save to reload.
//        </p>
//        <a
//          className="App-link"
//          href="https://reactjs.org"
//          target="_blank"
//          rel="noopener noreferrer"
//        >
//          Learn React
//        </a>
//      </header>
//    </div>
//  );
//}

export default App;
